exports.config = {
  directConnect: true,

  capabilities: {
    'browserName': 'chrome'
  },

  framework: 'jasmine',

   specs: ['../test/Test1.js','../test/Test2.js'],

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
};