require ('../pageObjects/Payment.js');

let interests_page = function () {

    this.xboxRadio = element(by.css('[value=xbox]'));
    this.psRadio = element(by.css('[value=ps]'));
    this.resultField = element(by.className('ng-binding'));

    this.nextPage = function () {
        element(by.className('btn btn-block btn-info')).click();
        return require('./Payment.js');
    };

};
module.exports = new interests_page();