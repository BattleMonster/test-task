require ('../pageObjects/Interests.js');
require ('../pageObjects/Payment.js');

let profile_page = function () {

    this.nameField = element(by.css('[name=name]'));
    this.emailField = element(by.css('[name=email]'));
    this.resultField = element(by.className('ng-binding'));

    this.enterName = function (name) {
        this.nameField.sendKeys(name);
    };

    this.enterEmail = function (email) {
        this.emailField.sendKeys(email);
    };

    this.writesmth = function () {
        document.write('something');
    };

    this.goToInterestsPage = function () {
        element(by.css('a:nth-child(2)')).click().click();
        return require('./Interests.js');
    };

    this.goToPaymentPage = function () {
        element(by.css('a:nth-child(3)')).click().click();
        return require('./Payment.js');
    };

    this.nextPage = function () {
        element(by.className('btn btn-block btn-info')).click();
        return require('./Interests.js');
    };

};
module.exports = new profile_page();