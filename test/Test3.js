        let user = {
            name: "person",
            id: 20
        };

        const delay = ms => new Promise(resolve => setTimeout(resolve, ms));

        async function flakyTest(data) {
            await edit(data);
            const actual = await get(user);
            const expected = await transform(data); //добавленно await для ожидания выполнения ф-ции transform
            if (actual == expected) { //реализация вместо deepEquals
                console.log("equals")
            } else {
                console.log("not equals");
            }
        }

        async function edit (data) {
            data.id = 21;
        }

        async function get (data) {
            return data.id;
        }

        async function transform (data){
            delay(1000); //эмуляция задержки выполнения
            return 21;
        }

        flakyTest(user);