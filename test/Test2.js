describe('Second Task', function () {
    let Request = require("request");
    it('Response code 200', function (done) {
        let KEY = "16821336-59f7a3769078a99d39d807613";
        let ID = "1";
        Request.get({
            "headers": {
                "content-type": "application/json"
            },
            "url": "https://pixabay.com/api/?key=" + KEY + "&id=" + ID
        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }
            console.log("Body : ");
            console.dir(JSON.parse(body));

            console.log("\n\n\nResponse Code :" + response.statusCode)
            expect(response.statusCode).toBe(200)

            done();
        });
    });

    it('No video', function (done) {
        let KEY = "16821336-59f7a3769078a99d39d807613";
        let ID = "1";
        Request.get({
            "headers": {
                "content-type": "application/json"
            },
            "url": "https://pixabay.com/api/videos/?key=" + KEY + "&id=" + ID
        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }
         
            expect(response.statusCode).toBe(404)

            done();
        });
    });

    it('Fake key', function (done) {
        let KEY = "6853";
        let ID = "1";
        Request.get({
            "headers": {
                "content-type": "application/json"
            },
            "url": "https://pixabay.com/api/?key=" + KEY + "&id=" + ID
        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }

            console.log("\n\n\nResponse Code :" + response.statusCode)
            expect(response.statusCode).toBe(400)

            done();
        });
    });

    it('Body check', function (done) {
        let KEY = "16821336-59f7a3769078a99d39d807613";
        let ID = "1";
        let LANG = "ru";
        Request.get({
            "headers": {
                "content-type": "application/json"
            },
            "url": "https://pixabay.com/api/?key=" + KEY + "&id=" + ID + "&lang=" + LANG
        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }

            JSON.parse(body, function (key, value) {
                if (key == "type") {
                    expect(value).toBe("photo");
                }
            });

            console.log("\n\n\nResponse Code :" + response.statusCode)
            expect(response.statusCode).toBe(200)

            done();
        });
    });

    it('q symbol limit check', function (done) {
        let KEY = "16821336-59f7a3769078a99d39d807613";
        let Q101 = "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111";
        Request.get({
            "headers": {
                "content-type": "application/json"
            },
            "url": "https://pixabay.com/api/?key=" + KEY + "&q=" + Q101
        }, (error, response, body) => {
            if (error) {
                return console.dir(error);
            }

            console.log("\n\n\nResponse Code :" + response.statusCode)
            expect(response.statusCode).toBe(400)

            done();
        });
    });
});