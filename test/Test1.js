describe('First Task', function () {

    beforeEach(function () {
        browser.get('http://www.way2automation.com/angularjs-protractor/multiform/#/form/profile');
    });

    let profile_page = require('../pageObjects/Profile.js');

    it('Profile input test', () => {

        let textForName = 'My name';
        let textForEmail = 'My email';

        profile_page.enterName(textForName);
        profile_page.enterEmail(textForEmail);

        expect(profile_page.resultField.getText()).toContain(textForName);
        expect(profile_page.resultField.getText()).toContain(textForEmail);

    });

    it('Test triming', () => {

        let textSpacedName = '     testName';
        let expectedName = '{"name":"testName"}';

        profile_page.enterName(textSpacedName);

        let tmp = profile_page.resultField.getText();

        let trimedText = tmp.then(function (text) {
            return text.trim();
        })

        expect(trimedText).toEqual(expectedName);
    });

    it('Try to go to Interests page', () => {

        profile_page.nextPage();
        let interestsPage = profile_page.goToInterestsPage();
        expect(interestsPage.xboxRadio.isPresent()).toBe(true);

    });

    it('To the Payment page', () => {
        profile_page.goToPaymentPage();
        let paymentPage = profile_page.goToPaymentPage();
        expect(paymentPage.heart.isPresent()).toBe(true);
    });

    it('Varify that xbox is choosen', () => {
        profile_page.goToInterestsPage();
        let interestsPage = profile_page.goToInterestsPage();
        interestsPage.xboxRadio.click();
        expect(interestsPage.resultField.getText()).toContain('xbox');
    });

    it('Try to sumbit data', () => {
        profile_page.goToPaymentPage();
        let paymentPage = profile_page.goToPaymentPage();
        paymentPage.submitButton.click();

        let EC = protractor.ExpectedConditions;
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();
    });

    it('SmokeTest', () => {
        let textForName = 'My name';
        let textForEmail = 'My email';

        profile_page.enterName(textForName);
        profile_page.enterEmail(textForEmail);

        let interestsPage = profile_page.nextPage();

        interestsPage.psRadio.click();

        let paymentPage = interestsPage.nextPage();

        expect(paymentPage.resultField.getText()).toContain(textForName);
        expect(paymentPage.resultField.getText()).toContain(textForEmail);
        expect(paymentPage.resultField.getText()).toContain('ps');

        paymentPage.submitButton.click();

        let EC = protractor.ExpectedConditions;
        browser.wait(EC.alertIsPresent(), 5000);
        browser.switchTo().alert().accept();

    });

});